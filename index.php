<?php

require_once('animal.php');

$sheep = new animal("Shaun");

echo "Name: " . $sheep->name . "<br>";
echo "Legs: " . $sheep->legs . "<br>";
echo "Cold Blooded: " . $sheep->cold_blooded . "<br>";
echo "<br>";


require_once('frog.php');
$kodok = new Frog("buduk");
echo "Name: " . $kodok->name . "<br>";
echo "Legs: " . $kodok->legs . "<br>";
echo "Cold Blooded: " . $kodok->cold_blooded . "<br>";
echo "Jump: "; 
$kodok->jump(); // "hop hop"
echo "<br>";

require_once('ape.php');
$kera = new Ape("Kera Sakti");
echo "Name: " . $kera->name . "<br>";
echo "Legs: " . $kera->legs . "<br>";
echo "Cold Blooded: " . $kera->cold_blooded . "<br>";
echo "Yell: ";
$kera->yell(); // "Auooo"